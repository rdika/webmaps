# webmaps

- webmaps 1
Using folium to make an interactive map for the location of volcano eruption

jupyter notebook:
https://nbviewer.jupyter.org/urls/gitlab.com/rdika/webmaps/raw/master/Volcano_eruption_-webmaps_exercise_1.ipynb


- webmaps 2
interactive map to visualize the name, type and the location of volcanoes in 
Indonesia

jupyter notebook: 
https://nbviewer.jupyter.org/urls/gitlab.com/rdika/webmaps/raw/master/Indonesia_s_Volcano_-_webmaps_exercise_2.ipynb